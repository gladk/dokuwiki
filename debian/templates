Template: dokuwiki/system/configure-webserver
Type: multiselect
__Choices: apache2, lighttpd
Default: apache2
_Description: Web server(s) to configure automatically:
 DokuWiki runs on any web server supporting PHP, but only listed
 web servers can be configured automatically.
 .
 Please select the web server(s) that should be configured
 automatically for DokuWiki.

Template: dokuwiki/system/restart-webserver
Type: boolean
Default: true
_Description: Should the web server(s) be restarted now?
 In order to activate the new configuration, the reconfigured web
 server(s) have to be restarted.

Template: dokuwiki/system/documentroot
Type: string
Default: /dokuwiki
_Description: Wiki location:
 Specify the directory below the server's document root from which
 DokuWiki should be accessible.

Template: dokuwiki/system/accessible
Type: select
Default: localhost only
_Description: Authorized network:
 Wikis normally provide open access to their content, allowing anyone
 to modify it. Alternatively, access can be restricted by IP address.
 .
 If you select "localhost only", only people on the local host (the machine
 the wiki is running on) will be able to connect. "local network" will
 allow people on machines in a local network (which you will need to
 specify) to talk to the wiki. "global" will allow anyone, anywhere, to
 connect to the wiki.
 .
 The default is for site security, but more permissive settings should
 be safe unless you have a particular need for privacy.
__Choices: localhost only, local network, global

Template: dokuwiki/system/localnet
Type: string
Default: 10.0.0.0/24
_Description: Local network:
 The specification of your local network should either be
 an IP network in CIDR format (x.x.x.x/y) or a domain specification (like
 .example.com).
 .
 Anyone who matches this specification will be given full and complete
 access to DokuWiki's content.

Template: dokuwiki/system/purgepages
Type: boolean
Default: false
_Description: Purge pages on package removal?
 By default, DokuWiki stores all its pages in a file database in
 /var/lib/dokuwiki.
 .
 Accepting this option will leave you with a tidier system when the
 DokuWiki package is removed, but may cause information loss if you have an
 operational wiki that gets removed.

Template: dokuwiki/system/writeconf
Type: boolean
Default: false
_Description: Make the configuration web-writeable?
 DokuWiki includes a web-based configuration interface. To be usable, it
 requires the web server to have write permission to the configuration
 directory.
 .
 Accepting this option will give the web server write permissions on the
 configuration directory and files.
 .
 The configuration files will still be readable and editable by hand
 regardless of whether or not you accept this option.

Template: dokuwiki/system/writeplugins
Type: boolean
Default: false
_Description: Make the plugins directory web-writeable?
 DokuWiki includes a web-based plugin installation interface. To be usable,
 it requires the web server to have write permission to the plugins directory.
 .
 Accepting this option will give the web server write permissions to the
 plugins directory.
 .
 Plugins can still be installed by hand regardless of whether or not you
 accept this option.

Template: dokuwiki/wiki/title
Type: string
Default: Debian DokuWiki
_Description: Wiki title:
 The wiki title will be displayed in the upper right corner of the default
 template and on the browser window title.

Template: dokuwiki/wiki/license
Type: select
__Choices: CC0 "No Rights Reserved", CC Attribution, CC Attribution-ShareAlike, GNU Free Documentation Licence, CC Attribution-NonCommercial, CC Attribution-NonCommercial-ShareAlike
Choices-C: cc-zero, cc-by, cc-by-sa, gnufdl, cc-by-nc, cc-by-nc-sa
Default: cc-by-sa
_Description: Wiki license:
 Please choose the license you want to apply to your wiki content. If none of
 these licenses suits your needs, you will be able to add your own to the file
 /etc/dokuwiki/license.php and to refer it in the main configuration file
 /etc/dokuwiki/local.php when the installation is finished.
 .
 Creative Commons "No Rights Reserved" is designed to waive as many rights as
 legally possible.
 .
 CC Attribution is a permissive license that only requires licensees to
 give credit to the author.
 .
 CC Attribution-ShareAlike and GNU Free Documentation License are
 copyleft-based free licenses (requiring modifications to be released under
 similar terms).
 .
 CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike
 are non-free licenses, in that they forbid commercial use.

Template: dokuwiki/wiki/acl
Type: boolean
Default: true
_Description: Enable ACL?
 Enable this to use an Access Control List for restricting what the users of
 your wiki may do.
 .
 This is a recommended setting because without ACL support you will not have
 access to the administration features of DokuWiki.

Template: dokuwiki/wiki/superuser
Type: string
Default: admin
_Description: Administrator username:
 Please enter a name for the administrator account, which will be able to
 manage DokuWiki's configuration and create new wiki users. The username
 should be composed of lowercase ASCII letters only.
 .
 If this field is left blank, no administrator account will be created now.

Template: dokuwiki/wiki/fullname
Type: string
Default: DokuWiki Administrator
_Description: Administrator real name:
 Please enter the full name associated with the wiki administrator account.
 This name will be stored in the wiki password file as an informative
 field, and will be displayed with the wiki page changes made by the
 administrator account.

Template: dokuwiki/wiki/email
Type: string
Default: webmaster@localhost
_Description: Administrator email address:
 Please enter the email address associated with the wiki administrator account.
 This address will be stored in the wiki password file, and may be used
 to get a new administrator password if you lose the original.

Template: dokuwiki/wiki/password
Type: password
_Description: Administrator password:
 Please choose a password for the wiki administrator.

Template: dokuwiki/wiki/confirm
Type: password
_Description: Re-enter password to verify:
 Please enter the same "admin" password again to verify
 you have typed it correctly.

Template: dokuwiki/wiki/failpass
Type: error
_Description: Password input error
 The two passwords you entered were not the same. Please try again.

Template: dokuwiki/wiki/policy
Type: select
Default: public
_Description: Initial ACL policy:
 Please select what initial ACL configuration should be set up to match
 the intended usage of this wiki:
  "open":   both readable and writeable for anonymous users;
  "public": readable for anonymous users, writeable for registered users;
  "closed": readable and writeable for registered users only.
 .
 This is only an initial setup; you will be able to adjust the ACL rules later.
__Choices: open, public, closed
